const express = require('express');
const app = express();
const port = 8080;
const formidable = require('express-formidable');

const mysql = require('mysql');

const createJS = require('./tools/create');
const deleteJS = require('./tools/delete');
const insertJS = require('./tools/insert');
const removeJS = require('./tools/remove');
const updateJS = require('./tools/update');

const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "database_api"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

app.use(formidable());

app.post('/create', (req, res) => {
    let form = req.fields.form;
    createJS.run(con, JSON.parse(form), function (response) {
        res.send(response);
    });
});
app.post('/delete', (req, res) => {
    let form = req.fields.form;
    deleteJS.run(con, JSON.parse(form), function (response) {
        res.send(response);
    });
});
app.post('/insert', (req, res) => {
    let form = req.fields.form;
    insertJS.run(con, JSON.parse(form), function (response) {
        res.send(response);
    });
});
app.post('/remove', (req, res) => {
    let form = req.fields.form;
    removeJS.run(con, JSON.parse(form), function (response) {
        res.send(response);
    });
});
app.post('/update', (req, res) => {
    let form = req.fields.form;
    updateJS.run(con, JSON.parse(form), function (response) {
        res.send(response);
    });
});


app.listen(port, () => console.log(`API on port ${port}!`));