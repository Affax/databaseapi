exports.run = function(con, json, response) {
    if (json.name != null) {
        let sql = 'DROP TABLE ' + json.name;
        con.query(sql, function (err, result) {
            if (err) {
                response('{ "type":"error", "result":"' + err + '" }');
            } else {
                response('{ "type":"success", "result":"Table deleted." }');
            }
        });
    }
};