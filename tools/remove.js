exports.run = function(con, json, response) {
    if (json.name != null && json.where != null) {
        let sql = 'DELETE FROM ' + json.name + " WHERE " + json.where + ";";
        con.query(sql, function (err, result) {
            if (err) {
                response('{ "type":"error", "result":"' + err + '" }');
            } else {
                response('{ "type":"success", "result":"Items removed." }');
            }
        });
    }
};