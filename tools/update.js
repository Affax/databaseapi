exports.run = function(con, json, response) {
    if (json.items != null && json.name != null && json.where != null) {
        let sql = 'UPDATE ' + json.name + ' SET ';
        let i = 0;
        json.items.forEach(function (item) {
            if (json.items.length === i + 1) {
                sql += item.name + "='" + item.value + "'";
            } else {
                sql += item.name + "='" + item.value + "',";
            }
            i++;
        });
        sql += ' WHERE ' + json.where + ';';
        console.log(sql);
        con.query(sql, function (err, result) {
            if (err) {
                response('{ "type":"error", "result":"' + err + '" }');
            } else {
                response('{ "type":"success", "result":"Data updated." }');
            }
        });
    }
};