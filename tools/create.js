exports.run = function(con, json, response) {
    if (json.items != null && json.name != null) {
        let sql = 'CREATE TABLE ' + json.name + ' (\n';
        let i = 0;
        json.items.forEach(function (item) {
            if (json.items.length === i + 1) {
                sql += item.name + " " + item.type + " " + item.extra + "\n";
            } else {
                sql += item.name + " " + item.type + " " + item.extra + ",\n";
            }
            i++;
        });
        sql += ');';
        con.query(sql, function (err, result) {
            if (err) {
                response('{ "type":"error", "result":"' + err + '" }');
            } else {
                response('{ "type":"success", "result":"Table created." }');
            }
        });
    }
};