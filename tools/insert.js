exports.run = function(con, json, response) {
    if (json.items != null && json.name != null) {
        let sql = 'INSERT INTO ' + json.name + ' (';
        let i = 0;
        json.items.forEach(function (item) {
            if (json.items.length === i + 1) {
                sql += + item.name;
            } else {
                sql += + item.name + ",";
            }
            i++;
        });
        sql += ') VALUES (';
        i = 0;
        json.items.forEach(function (item) {
            if (json.items.length === i + 1) {
                sql += "'" + item.value + "'";
            } else {
                sql += "'" + item.value + "',";
            }
            i++;
        });
        sql += ');';
        console.log(sql);
        con.query(sql, function (err, result) {
            if (err) {
                response('{ "type":"error", "result":"' + err + '" }');
            } else {
                response('{ "type":"success", "result":"Data inserted." }');
            }
        });
    }
};